package com.example.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.commonutils.RecyclerAdapter;
import com.example.isspass.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ISSFragment extends Fragment {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private RecyclerAdapter recyclerAdapter;

    public static ISSFragment newInstance() {

        Bundle args = new Bundle();

        ISSFragment fragment = new ISSFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public ISSFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_iss, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //setting the recycler view
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerAdapter = new RecyclerAdapter();
        recyclerView.setAdapter(recyclerAdapter);
    }

    public void updateRecyclerViewData(String data) {
        recyclerAdapter.updateData(data);
        recyclerView.scrollToPosition(recyclerAdapter.getItemCount() - 1);
    }
}
