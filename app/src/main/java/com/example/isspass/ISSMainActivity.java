package com.example.isspass;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.commonutils.LocationCallBack;
import com.example.commonutils.LocationUpdatesBroadcastReceiver;
import com.example.commonutils.TimeStampConverter;
import com.example.commonutils.Utils;
import com.example.fragments.ISSFragment;
import com.example.models.ISSModelObject;
import com.example.models.Request;
import com.example.servicerequest.ApiClient;
import com.example.servicerequest.ApiInterface;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ISSMainActivity extends AppCompatActivity implements LocationCallBack, Callback<ISSModelObject> {

    private static final String TAG = ISSMainActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1001;
    /**
     * The desired interval for location updates.
     */
    private static final long UPDATE_INTERVAL = 10000;
    /**
     * The fastest rate for active location updates.
     */
    private static final long FASTEST_UPDATE_INTERVAL = 5000;
    /**
     * The max time before batched results are delivered by location services.
     */
    private static final long MAX_WAIT_TIME = UPDATE_INTERVAL * 5;
    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private LocationRequest mLocationRequest;

    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    private ApiInterface mApiInterface;

    private ISSFragment hostedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issmain);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        createLocationRequest();

        loadInitialFragment();

        //Instantiating the service object
        mApiInterface = ApiClient.getClientInstance().create(ApiInterface.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Check if the user revoked runtime permissions.
        if (!checkPermissions()) {
            requestPermissions();
        } else {
            requestLocationUpdates();
        }
    }

    /**
     * Loads the initial fragment
     */
    private void loadInitialFragment() {
        hostedFragment = ISSFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, hostedFragment)
                .commit();
    }

    /**
     * Sets up the location request.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);

        // Sets the fastest rate for active location updates.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Sets the maximum time
        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);
        //start listening for location call backs
        Utils.getInstance().instantiateCallBack(this);
    }

    /**
     * starts location request
     */
    private void requestLocationUpdates() {
        try {
            Log.i(TAG, "Starting location updates");
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, getPendingIntent());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes location updates
     */
    private void removeLocationUpdates() {
        Log.i(TAG, "Removing location updates");
        mFusedLocationClient.removeLocationUpdates(getPendingIntent());
    }

    private PendingIntent getPendingIntent() {
        Intent intent = new Intent(this, LocationUpdatesBroadcastReceiver.class);
        intent.setAction(LocationUpdatesBroadcastReceiver.ACTION_PROCESS_UPDATES);
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    protected void onStop() {
        super.onStop();
        removeLocationUpdates();
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(R.id.issmain_activity),
                    R.string.permission_needed_str,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(ISSMainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(ISSMainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                requestLocationUpdates();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                Snackbar.make(
                        findViewById(R.id.issmain_activity),
                        R.string.permission_denied,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        }
    }

    @Override
    public void locationReceived(String latitude, String longitude, String currentTime) {
        Toast.makeText(this, "Lat: " + latitude + " Long: " + longitude + " +Time: " + currentTime, Toast.LENGTH_SHORT).show();
        Call<ISSModelObject> issServiceCall = mApiInterface.getIssResponse(latitude, longitude);
        issServiceCall.enqueue(this);
    }

    @Override
    public void onResponse(Call<ISSModelObject> call, Response<ISSModelObject> response) {
        Log.i(TAG, "Response from ISS:::::" + response);
        if (response.isSuccessful()) {
            Request requestObj = response.body().getRequest();
            List<com.example.models.Response> responseObj = response.body().getResponse();
            StringBuilder sb = new StringBuilder();
            sb.append("Altitude: " + requestObj.getAltitude());
            sb.append("\n");
            sb.append("Date time: " + TimeStampConverter.getInstance().getDateFromUnixTimeStamp(requestObj.getDatetime()));
            sb.append("\n");
            sb.append("Latitude: " + requestObj.getLatitude());
            sb.append("\n");
            sb.append("Longitude: " + requestObj.getLongitude());
            sb.append("\n");
            sb.append("Passes: " + requestObj.getPasses());
            sb.append("\n");
            sb.append("============");
            sb.append("\n");
            int count = 1;
            for (com.example.models.Response res : responseObj) {
                sb.append(count + ". Duration: " + res.getDuration() + " Seconds");
                sb.append("\n");
                sb.append("Rise time: " + TimeStampConverter.getInstance().getDateFromUnixTimeStamp(res.getRisetime()));
                sb.append("\n");
                count++;
            }
            //updates the recycler view data
            hostedFragment.updateRecyclerViewData(sb.toString());
        }
    }

    @Override
    public void onFailure(Call<ISSModelObject> call, Throwable t) {
        Log.i(TAG, ":::::Request failed from ISS:::::");
    }
}
