package com.example.servicerequest;

import com.example.models.ISSModelObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by pawars on 12/3/2017.
 */

public interface ApiInterface {

    @GET("/iss-pass.json")
    Call<ISSModelObject> getIssResponse(@Query("lat") String latitude, @Query("lon") String longitude);
}
