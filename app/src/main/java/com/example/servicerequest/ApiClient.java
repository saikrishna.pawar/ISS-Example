package com.example.servicerequest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pawars on 12/3/2017.
 */

public class ApiClient {
    public static final String BASE_URL = "http://api.open-notify.org";
    private static Retrofit retrofit = null;

    private ApiClient() {

    }

    public static Retrofit getClientInstance() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
