package com.example.commonutils;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.example.isspass.R;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by pawars on 12/3/2017.
 */

public class Utils {

    private LocationCallBack mCallBack;
    private static Utils mUtils;

    private Utils() {

    }

    public static Utils getInstance() {
        if (mUtils == null) {
            mUtils = new Utils();
        }
        return mUtils;
    }

    public void instantiateCallBack(LocationCallBack mCallBack) {
        this.mCallBack = mCallBack;
    }

    /**
     * Returns text for reporting about a list
     *
     * @param locations List of {@link Location}s.
     */
    private String getLocationResultText(Context context, List<Location> locations) {
        if (locations.isEmpty()) {
            return context.getString(R.string.unknown_location);
        }
        StringBuilder sb = new StringBuilder();
        for (Location location : locations) {
            sb.append("(");
            sb.append(location.getLatitude());
            sb.append(", ");
            sb.append(location.getLongitude());
            sb.append(")");
            sb.append("\n");
        }
        return sb.toString();
    }

    public void setLocationUpdatesResult(Context context, List<Location> locations) {
        Log.i("Location Updates", "Location:::::" + getLocationResultText(context, locations));
        if (locations.size() > 0) {
            Location location = locations.get(0);
            mCallBack.locationReceived(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()), DateFormat.getDateTimeInstance().format(new Date()));
        }
    }
}
